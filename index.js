// Load the expressjs module into our application and saved it in a variable called express
// Syntax: require("package");
const express = require("express");
// localhost port number
const port = 4000;
// app is our server
// create an application that uses express and stores it as app.
const app = express();

// middleware
// express.json() is a method which allows us to handle the streaming of data and automatically parse the incoming JSON from our request body.
app.use(express.json());

// mock data
let users = [
	{
		username: "TStark3000",
		email: "starkindustries@mail.com",
		password: "notPeterParker"
	},
	{
		username: "ThorThuder",
		email: "loveAndThunder@mail.com",
		password: "iLoveStormBreaker"
	}
];

let items = [
	{
		name: "Mjolnir",
		price: 50000,
		isActive: true
	},
	{
		name: "Vibranium Shield",
		price: 70000,
		isActive: true
	}
];

// Express has methods to use as routes corresponding to HTTP methods
// Syntax: app.method(<endpoint>, <function for request and response>)
app.get('/', (request, response) => {
	// response.end() actually combines writeHead() and end()
	response.send('Hello from my first expressJS API');
	// response.status(200).send("message");
})

// MINI Activity
app.get('/greeting', (request, response) => {
	response.send('Hello from Batch203-Parce');
});

// Retrieval of the users in mock database
app.get('/users', (request, response) => {
	// res.send already stringifies for you
	response.send(users);
});

// Adding a new user
app.post('/users', (request, response) => {
	console.log(request.body); // result: {} emmpty object

	// simulate creating a new user document
	let newUser = {
		username: request.body.username,
		email: request.body.email,
		password: request.body.password
	};

	// adding newUser to the existing array
	users.push(newUser);
	console.log(users);

	// send the updated users array in the client
	response.send(users);
});

// Delete Method
app.delete('/users', (request, response) => {
	// deleting the last user of the users array
	users.pop();
	response.send(users);
});

// PUT Method
// Update user's password
// :index - wildcard
// url: localhost:4000/users/0
app.put('/users/:index', (request, response) => {
	console.log(request.body); // update password
	// result {}

	// an object that contain the value of URL params
	console.log(request.params); // refer to the wildcard in the endpoint
	// result: {index: 0}

	// parseInt the value of the number coming from req.params
	// [0] turns into [0]
	let index = parseInt(request.params.index);

	// users[0].password
	users[index].password = request.body.password;

	//send the updated user
	response.send(users[index]);
});


	// [ACTIVITY]
	
	// Endpoint: /items

 	// >> Create a new route to get and send items array in the client (GET ALL ITEMS) 

    app.get('/items', (request, response) => {
	response.send(items);
	})


    // >> Create a new route to create and add a new item object in the items array (CREATE ITEM)
    //     >> send the updated items array in the client
    //     >> check the post method route for our users for reference

	app.post('/items', (request, response) => {
	console.log(request.body);

	let newItem = {
		name: request.body.name,
		price: request.body.price,
		isActive: true
	};
	items.push(newItem);
	console.log(items);

	response.send(items);
	});


    // >> Create a new route which can update the price of a single item in the array (UPDATE ITEM)
    //     >> pass the index number of the item that you want to update in the request params
    //     >> add the price update in the request body
    //     >> reassign the new price from our request body
    //     >> send the updated item in the client

	app.put('/items/:index', (request, response) => {
	console.log(request.body); 

	console.log(request.params); 

	let index = parseInt(request.params.index);

	items[index].price = request.body.price;

	response.send(items[index]);
	});



// port listener
app.listen(port, () => console.log(`Server is running at port ${port}`));
